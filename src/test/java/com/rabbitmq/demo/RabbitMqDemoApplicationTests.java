package com.rabbitmq.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.rabbitmq.demo.dto.Order;
import com.rabbitmq.demo.publisher.OrderPublisher;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RabbitMqDemoApplication.class})
@ActiveProfiles(profiles = "test")
public class RabbitMqDemoApplicationTests {

	@Autowired
    private ApplicationContext applicationContext;
	
	@Autowired
	private OrderPublisher publish;
	

    @Test
    public void test() {
        assertNotNull(applicationContext);
    }
    
    
    
//    = (Order)JSON.deserializeStrict({"name":"Burger","qty":2,"price":40},Order.class);
    
    @Test
    public void testBookOrder() {
    	Order order = new Order();
    	order.setOrderId("123");
    	order.setName("Pizza");
    	order.setQty(1);
    	order.setPrice(180);
    	String msg = publish.bookOrder(order, "TGB");
    	
    	assertEquals(msg,"Success !!");
    }
    }




